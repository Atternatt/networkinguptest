/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.app.weatherlist;

import android.databinding.ObservableField;
import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import net.m2f.networkingup.weather.di.ActivityScope;
import net.m2f.networkingup.weather.domain.interactor.DefaultSubscriber;
import net.m2f.networkingup.weather.domain.interactor.WeatherUseCase;
import net.m2f.networkingup.weather.domain.interactor.UseCaseSubject;
import net.m2f.networkingup.weather.domain.model.Weather;
import net.m2f.networkingup.weather.retrypolicy.RetryIncremental;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

/**
 * ViewModel that controls the WeatherList search.
 *
 * @author Marc Moreno
 * @version 1.0
 */
@ActivityScope public class WeatherListViewModel {

  public final ObservableField<List<Weather>> weatherlist = new ObservableField<>();
  UseCaseSubject eventListUseCase;

  Observable<String> queryObservable;

  @Inject public WeatherListViewModel(UseCaseSubject eventListUseCase) {
    this.eventListUseCase = eventListUseCase;
    this.eventListUseCase.setRetryPolicy(new RetryIncremental(3,10, TimeUnit.SECONDS));
  }

  public void bind(Observable<String> query) {
    queryObservable = query;
    eventListUseCase.bind(queryObservable, new EventListViewModelInteractor());
  }

  /**
   * {@link DefaultSubscriber} implementations that reacts to {@link WeatherUseCase} emisions.
   */
  @RxLogSubscriber private class EventListViewModelInteractor
      extends DefaultSubscriber<List<Weather>> {

    @Override public void onNext(List<Weather> events) {
      Log.d("RESULTS", events.size() + "");
      weatherlist.set(events);
    }

    @Override public void onError(Throwable e) {
      super.onError(e);
    }

    @Override public void onCompleted() {
      super.onCompleted();
    }
  }
}
