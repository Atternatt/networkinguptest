/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.app.weatherlist;

import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import net.m2f.networkingup.weather.app.weatherlist.adapter.WeatherListAdapter;
import net.m2f.networkingup.weather.app.weatherlist.rx.RxMaterialSearchView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import net.m2f.networkingup.weather.domain.model.Weather;
import rx.Observable;

/**
 * Class that controls the behaviour between view and viewModel. This contains the
 * custom attributes and their interactions using {@link BindingAdapter}.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class WatherListViewModelBinding {

  /**
   * This will bind a {@link Observable<String>} from {@link MaterialSearchView} to
   * the {@link WeatherListViewModel} so this will be able to listen
   * for {@link MaterialSearchView}
   *
   * @param searchView the {@link MaterialSearchView}
   * @param weatherListViewModel the {@link WeatherListViewModel}
   */
  @BindingAdapter("search")
  public static void bindSearchViewToViewModel(MaterialSearchView searchView,
      WeatherListViewModel weatherListViewModel) {

    weatherListViewModel.bind(RxMaterialSearchView.queryTextChangeEvents(searchView)
        .debounce(250, TimeUnit.MILLISECONDS)); //this allows as to not overcharge requests while writing.
  }

  /**
   * This will set the list of weather objects to the recyclerview with a {@link WeatherListAdapter}.
   *
   * @param recyclerView the recyclerview
   * @param oldList the last list setted
   * @param newList the new list to set
   */
  @BindingAdapter("weatherList")
  public static void bindListToRecyclerView(RecyclerView recyclerView, List<Weather> oldList,
      List<Weather> newList) {

    if (oldList == null) {
      recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
      WeatherListAdapter adapter = new WeatherListAdapter(newList);
      recyclerView.setAdapter(adapter);
    } else {
      ((WeatherListAdapter) recyclerView.getAdapter()).setWeatherList(newList);
    }

  }
}
