/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.app.weatherlist;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import javax.inject.Inject;
import net.m2f.networkingup.weather.R;
import net.m2f.networkingup.weather.app.ui.BaseActivity;
import net.m2f.networkingup.weather.databinding.ActivityWeatherListBinding;
import net.m2f.networkingup.weather.di.components.DaggerWeatherComponent;

/**
 * Activity that shows a list of Weathers.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class WeatherListActivity extends BaseActivity {

  @Inject WeatherListViewModel weatherListViewModel;

  private ActivityWeatherListBinding binding;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    binding = DataBindingUtil.setContentView(this, R.layout.activity_weather_list);

    initializeInjector();

    binding.setWeatherViewModel(weatherListViewModel);
  }

  @Override protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    binding.toolbar.inflateMenu(R.menu.search);
    binding.searchView.setMenuItem(binding.toolbar.getMenu().findItem(R.id.action_search));
  }

  private void initializeInjector() {
    DaggerWeatherComponent.builder()
        .applicationComponent(getApplicationComponent())
        .activityModule(getActivityModule())
        .build()
        .inject(this);
  }
}
