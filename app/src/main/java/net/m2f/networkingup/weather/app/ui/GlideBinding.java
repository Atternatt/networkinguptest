/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.app.ui;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import com.bumptech.glide.Glide;

/**
 * Binding class to download and set weather icons in imageview
 * @author Marc Moreno
 * @version 1
 */
public final class GlideBinding {

  private GlideBinding() {
  }

  /**
   * Every {@link ImageView} with a custom atribute "weatherIcon" will call this method.
   * @param view the imageView
   * @param iconId the icon id
   */
  @BindingAdapter({"weatherIcon"})
  public static void loadImage(ImageView view, String iconId) {
    String url = String.format("http://openweathermap.org/img/w/%s.png",iconId);
    Glide.with(view.getContext()).load(url).into(view);
  }
}
