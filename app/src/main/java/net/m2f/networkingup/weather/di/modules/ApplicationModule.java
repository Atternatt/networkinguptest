/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.di.modules;

import android.content.Context;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import net.m2f.networkingup.weather.AndroidApplication;
import net.m2f.networkingup.weather.data.api.Api;
import net.m2f.networkingup.weather.data.api.ApiImpl;
import net.m2f.networkingup.weather.data.database.WeatherDatabase;
import net.m2f.networkingup.weather.data.database.OrmLiteDatabase;
import net.m2f.networkingup.weather.data.repository.WeatherDataRepository;
import net.m2f.networkingup.weather.domain.executor.PostExecutionThread;
import net.m2f.networkingup.weather.domain.repository.WeatherRepository;
import net.m2f.networkingup.weather.executor.UIThread;

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 *
 * @author Marc Moreno
 * @version 1.0
 */
@Module public class ApplicationModule {
  private final AndroidApplication application;

  public ApplicationModule(AndroidApplication application) {
    this.application = application;
  }

  @Provides @Singleton Context provideApplicationContext() {
    return this.application.getApplicationContext();
  }

  @Provides @Singleton PostExecutionThread providePostExecutionThread(UIThread uiThread) {
    return uiThread;
  }

  @Provides @Singleton WeatherDatabase provideEventDatabase(OrmLiteDatabase database) {
    return database;
  }

  @Provides @Singleton Api provideRestApi(ApiImpl api) {
    return api;
  }

  @Provides @Singleton WeatherRepository provideUserRepository(
      WeatherDataRepository weatherDataRepository) {
    return weatherDataRepository;
  }
}