/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.data.repository;

import net.m2f.networkingup.weather.data.repository.datasource.WeatherDataStore;
import net.m2f.networkingup.weather.data.repository.datasource.WeatherDataStoreFactory;
import net.m2f.networkingup.weather.domain.model.Weather;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.m2f.networkingup.weather.domain.repository.WeatherRepository;
import rx.Observable;

/**
 * Repository implementation that retrieve {@link Weather} related streams.
 *
 * @author Marc Moreno
 * @version 1.0
 */
@Singleton
public class WeatherDataRepository implements WeatherRepository {

    WeatherDataStoreFactory weatherDataStoreFactory;

    @Inject
    public WeatherDataRepository(WeatherDataStoreFactory weatherDataStoreFactory) {
        this.weatherDataStoreFactory = weatherDataStoreFactory;
    }

    /**
     * search for weather given a city name.
     *
     * @param query the city name
     * @return a list of weathers
     */
    @Override
    public Observable<List<Weather>> getAllEventsFromArtist(String query) {
        WeatherDataStore dataStore = weatherDataStoreFactory.createCloudDataStore();
        WeatherDataStore databaseDataStore = weatherDataStoreFactory.createDataBaseStore();


        return Observable.concat(
                databaseDataStore.weatherList(query),
                dataStore.weatherList(query))
                .takeFirst(events -> !events.isEmpty());
    }
}
