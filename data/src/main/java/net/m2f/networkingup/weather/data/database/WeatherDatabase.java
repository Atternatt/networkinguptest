/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.data.database;

import java.util.List;
import net.m2f.networkingup.weather.data.database.model.WeatherDBO;
import net.m2f.networkingup.weather.domain.model.Weather;
import rx.Observable;

/**
 * Weather DataBase representation.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public interface WeatherDatabase {


    /**
     * Given an city name return a list of weathers.
     *
     * @param city the city
     * @return a {@link Observable} that emit a list of {@link WeatherDBO}.
     */
    Observable<List<WeatherDBO>> fetchWeather(String city);

    /**
     * Store a list of weather objects.
     *
     * @param list       the list of events.
     */
    void storeWeatherList(final List<Weather> list);

    /**
     * Store weather.
     *
     * @param weather      the weather
     */
    void storeWeather(final Weather weather);

}
