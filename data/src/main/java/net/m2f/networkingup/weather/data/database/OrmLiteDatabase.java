/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.data.database;

import android.content.Context;
import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import net.m2f.networkingup.weather.data.database.helper.OrmLiteDatabaseHelper;
import net.m2f.networkingup.weather.data.database.model.WeatherDBO;
import net.m2f.networkingup.weather.data.database.model.mapper.WeatherDboMapper;
import net.m2f.networkingup.weather.domain.model.Weather;
import rx.Observable;

/**
 * @author Marc Moreno
 * @version 1.0
 */
public class OrmLiteDatabase implements WeatherDatabase {

  private OrmLiteDatabaseHelper helper;
  private Context context;

  @Inject public OrmLiteDatabase(Context context) {
    this.context = context;
  }

  public OrmLiteDatabaseHelper getHelper() {
    if (helper == null) {
      helper = new OrmLiteDatabaseHelper(this.context);
    }
    return helper;
  }

  @RxLogObservable @Override public Observable<List<WeatherDBO>> fetchWeather(String city) {

    return Observable.create(subscriber -> {
      try {
        List<WeatherDBO> events =
            getHelper().getDao().queryBuilder().where().eq("city", city).query();
        if (!subscriber.isUnsubscribed()) {
          subscriber.onNext(events);
          subscriber.onCompleted();
        }
      } catch (SQLException e) {
        subscriber.onError(e);
      }
    });
  }

  @Override public void storeWeatherList(final List<Weather> list) {
    for (Weather weather : list) {
      storeWeather(weather);
    }
  }

  @Override public void storeWeather(final Weather weather) {
    try {
      Dao<WeatherDBO, Integer> dao = getHelper().getDao();
      WeatherDBO weatherDBO = WeatherDboMapper.transformToDBO(weather);
      dao.createIfNotExists(weatherDBO);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

}
