/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.data.api;

import net.m2f.networkingup.weather.data.api.entity.mapper.WeatherEntityMapper;
import net.m2f.networkingup.weather.data.database.WeatherDatabase;

import java.util.List;

import net.m2f.networkingup.weather.domain.model.Weather;
import retrofit2.adapter.rxjava.Result;
import rx.Observable;

/**
 * {@link Observable.Transformer} implementation that transforms a {@link WeatherResponse} to a {@link List<Weather>}
 * using {@link WeatherEntityMapper}.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class FromWeatherResponseToWeatherListWithDatabaseStoring implements Observable.Transformer<Result<WeatherResponse>, List<Weather>> {


    WeatherDatabase database;
    String city;

    public FromWeatherResponseToWeatherListWithDatabaseStoring(WeatherDatabase database) {
        this.database = database;
    }

    @Override
    public Observable<List<Weather>> call(Observable<Result<WeatherResponse>> volumeResponseObservable) {
        return volumeResponseObservable.map(eventResponseResult -> {
            WeatherResponse body = eventResponseResult.response().body();
            city = body.getCity();
            return body.getWeather();}
        )
                .flatMap(Observable::from)
                .map(entityWeather -> WeatherEntityMapper.transformWithCity(entityWeather,city))
                .toList()
                .doOnNext(weather -> database.storeWeatherList(weather));
    }

}
