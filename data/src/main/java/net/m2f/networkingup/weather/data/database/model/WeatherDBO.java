/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.data.database.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Marc on 23/6/16.
 */
@DatabaseTable(tableName = WeatherDBO.TABLE_NAME)
public class WeatherDBO extends BaseEntity {

  public static final String TABLE_NAME = "weather_cache";

  public static final String FIELD_ID = "id";
  public static final String FIELD_DESCRIPTION = "description";
  public static final String FIELD_ICON = "icon";
  public static final String FIELD_CITY = "city";


  @DatabaseField(columnName = FIELD_ID, generatedId = true)
  private int id;

  @DatabaseField(columnName = FIELD_DESCRIPTION)
  private String description;

  @DatabaseField(columnName = FIELD_ICON)
  private String icon;

  @DatabaseField(columnName = FIELD_CITY)
  private String city;

  public WeatherDBO() {
  }

  public WeatherDBO(String description, String icon, String city) {
    this.description = description;
    this.icon = icon;
    this.city = city;
  }

  public String getDescription() {
    return description;
  }

  public String getIcon() {
    return icon;
  }

  public String getCity() {
    return city;
  }
}
