/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.data.api;

import retrofit2.adapter.rxjava.Result;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * @author Marc Moreno
 * @version 1
 */
public interface Api {


  /** The app id, need to identify the client.*/
  String APP_ID = "2070ed36da1576a1002030565d9291ea";

  /**The endpoint.*/
  String BASE_URL = "http://api.openweathermap.org/data/2.5/";

  /**
   * Given an artist name return a list of events.
   * @param city the artist
   * @return a {@link Observable <EventResponse>} that emit EventResponses.
   */
  @GET("weather?appid="+ APP_ID)
  Observable<Result<WeatherResponse>> fetchCityWeather(@Query("q") String city);
}
