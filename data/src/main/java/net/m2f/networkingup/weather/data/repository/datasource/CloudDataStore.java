/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.data.repository.datasource;

import java.util.List;
import net.m2f.networkingup.weather.data.api.Api;
import net.m2f.networkingup.weather.data.api.FromWeatherResponseToWeatherListWithDatabaseStoring;
import net.m2f.networkingup.weather.data.database.WeatherDatabase;
import net.m2f.networkingup.weather.domain.model.Weather;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Cloud implementation of {@link WeatherDataStore}.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class CloudDataStore implements WeatherDataStore {

  Api api;
  WeatherDatabase database;

  public CloudDataStore(Api api, WeatherDatabase database) {
    this.api = api;
    this.database = database;
  }

  /**
   * {@inheritDoc}
   */
  @Override public Observable<List<Weather>> weatherList(String searchQuery) {

    return api.fetchCityWeather(searchQuery)
        .compose(new FromWeatherResponseToWeatherListWithDatabaseStoring(database))
        .subscribeOn(Schedulers.io());
  }
}
