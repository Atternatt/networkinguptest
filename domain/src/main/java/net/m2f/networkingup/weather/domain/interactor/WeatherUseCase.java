/*
 *  Copyright (c) 2016 Marc Moreno
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package net.m2f.networkingup.weather.domain.interactor;

import net.m2f.networkingup.weather.domain.executor.PostExecutionThread;

import java.util.List;

import javax.inject.Inject;

import net.m2f.networkingup.weather.domain.model.Weather;
import net.m2f.networkingup.weather.domain.repository.WeatherRepository;
import rx.Observable;

/**
 * {@link UseCaseSubject} implementation that retrieve a list of Weathers.
 *
 * @author Marc Moreno
 * @version 1.0
 */
public class WeatherUseCase extends UseCaseSubject<String, List<Weather>> {

  WeatherRepository weatherRepository;

  @Inject protected WeatherUseCase(WeatherRepository weatherRepository,
      PostExecutionThread postExecutionThread) {
    super(postExecutionThread);
    this.weatherRepository = weatherRepository;
  }

  @Override public Observable<List<Weather>> call(Observable<String> stringObservable) {
    return stringObservable.filter(query -> null != query && !query.isEmpty())
        .switchMap(query -> weatherRepository.getAllEventsFromArtist(query));
  }
}
